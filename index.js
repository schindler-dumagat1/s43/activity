// alert("Hello World")

let posts = [];
let count = 1;

//Add Post Data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault(); //prevent the page from loading

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});

	count++;

	showPosts(posts);
	alert('Successfully Added!');

})

const showPosts = (posts) => {

	let postsEntries = '';
	posts.forEach((post) => {

		postsEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`

	})

	document.querySelector('#div-post-entries').innerHTML = postsEntries;

}

//Editing a Post

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

}

//Deleting a Post
const deletePost = (id) => {

	let postToRemove = document.querySelector(`#post-${id}`);
	postToRemove.parentElement.removeChild(postToRemove);
	alert('Successfully Deleted!')

	for(let i = 0; i < posts.length; i++) {

		if(posts[i].id.toString() === id.toString()) {

			posts.splice(i, 1)

			// posts[i].title = document.querySelector('#txt-edit-title').value;
			// posts[i].body = document.querySelector('#txt-edit-body').value;

			// showPosts(posts);
			// alert('Successfull updated');

			break;

		}

	}
	console.log(posts)

}

//Update Post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	for(let i = 0; i < posts.length; i++) {

		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {

			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Successfull updated');

			break;

		}

	}

})